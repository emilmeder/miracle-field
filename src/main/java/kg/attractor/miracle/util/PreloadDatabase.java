package kg.attractor.miracle.util;


import kg.attractor.miracle.model.*;
import kg.attractor.miracle.repository.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class PreloadDatabase {
    private final PasswordEncoder encoder;

    public PreloadDatabase(PasswordEncoder encoder) {

        this.encoder = encoder;
    }

    @Bean
    CommandLineRunner initDatabase(UserRepository userRepository, WordRepository wordRepository, GameRepository gameRepository){
        return (args) -> {
            gameRepository.deleteAll();
            userRepository.deleteAll();
            wordRepository.deleteAll();
            User admin = new User();
            admin.setName("admin");
            admin.setPassword(encoder.encode("123"));
            admin.setRole("ROLE_ADMIN");
            userRepository.save(admin);

            User user = new User();
            user.setName("user");
            user.setPassword(encoder.encode("123"));
            userRepository.save(user);

            User lolo = new User();
            lolo.setName("lolo");
            lolo.setPassword(encoder.encode("123"));
            userRepository.save(lolo);

            User pepe = new User();
            pepe.setName("pepe");
            pepe.setPassword(encoder.encode("123"));
            userRepository.save(pepe);


            Word ferrari = Word.builder()
            .name("Ferrari")
            .description("Итальянский спорткар(Ferrari)")
            .build();
            wordRepository.save(ferrari);

            Word apple = Word.builder()
            .name("Apple")
            .description("Бренд телефонов(Apple)")
            .build();
            wordRepository.save(apple);

            Word spring = Word.builder()
            .name("Spring")
            .description("Весна на английском(Spring)")
            .build();
            wordRepository.save(spring);



        };
    }
}
