package kg.attractor.miracle.dto;
import kg.attractor.miracle.model.*;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GameDTO {
    private Integer id;
    private Integer attempts;
    private Integer points;
    private String alphabet;
    private String user_word;
    private UserDTO userDTO;
    private WordDTO wordDTO;

    public static GameDTO from(Game game){
        return GameDTO.builder().id(game.getId()).userDTO(UserDTO.from(game.getUser())).wordDTO(WordDTO.from(game.getWord())).attempts(game.getAttempts()).points(game.getPoints()).alphabet(game.getAlphabet()).user_word(game.getUser_word()).build();
    }
}
