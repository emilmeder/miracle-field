package kg.attractor.miracle.dto;
import kg.attractor.miracle.model.Word;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class WordDTO {
    private String name;

    private String description;

    public static WordDTO from (Word word) {
        return WordDTO.builder().name(word.getName()).description(word.getDescription()).build();
    }
}
