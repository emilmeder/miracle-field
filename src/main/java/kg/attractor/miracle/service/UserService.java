package kg.attractor.miracle.service;

import kg.attractor.miracle.dto.UserDTO;
import kg.attractor.miracle.dto.WordDTO;
import kg.attractor.miracle.model.User;
import kg.attractor.miracle.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserService {
    private UserRepository userRepository;
    private final PasswordEncoder encoder;

    public void addUser(UserDTO userDTO){
        userRepository.save(User.builder().name(userDTO.getName()).password(encoder.encode(userDTO.getPassword())).build());
    }

    public List<UserDTO> findAllUsers() {

        return userRepository.findAll().stream().map(UserDTO::from).collect(Collectors.toList());
    }

    public UserDTO findUserByName(String name){

        return UserDTO.from(userRepository.findUserByName(name).get());
    }

    public UserDTO findUserById(Integer id){

        return UserDTO.from((userRepository.findUserById(id).get()));
    }

}
