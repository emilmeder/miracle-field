package kg.attractor.miracle.service;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import kg.attractor.miracle.dto.*;
import kg.attractor.miracle.model.*;
import kg.attractor.miracle.repository.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class WordService {
    private WordRepository wordRepository;

    public void addWord(WordDTO wordDTO){
        wordRepository.save(Word.builder().name(wordDTO.getName()).description(wordDTO.getDescription()).build());
    }

    public List<WordDTO> findAllWords() {
        return wordRepository.findAll().stream().map(WordDTO::from).collect(Collectors.toList());
    }

    public WordDTO findWordByName(String name){
        return WordDTO.from(wordRepository.findByName(name).get());
    }

    public WordDTO randomWord(){
        Random r = new Random();
        List<Word> words = wordRepository.findAll();
        return WordDTO.from(words.get(r.nextInt(words.size())));
    }
}
