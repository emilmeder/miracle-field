package kg.attractor.miracle.service;
import kg.attractor.miracle.dto.*;
import kg.attractor.miracle.repository.*;
import kg.attractor.miracle.model.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
public class GameService {
    private UserRepository userRepository;
    private GameRepository gameRepository;
    private WordRepository wordRepository;

    public List<GameDTO> findAllGames() {
        return gameRepository.findAll().stream().map(GameDTO::from).collect(Collectors.toList());
    }

    public GameDTO findGameById(Integer id){
        return GameDTO.from(gameRepository.findById(id).get());
    }

    public List<Game> findGameByUserId(Integer id){
        return gameRepository.findGameByUserId(id);
    }

    public GameDTO saveGame(UserDTO userDTO, WordDTO wordDTO){
        String userWord = "";
        for(int i=0; i<wordDTO.getName().length(); i++){
            userWord = userWord.concat("X");
        }
        Game game = Game.builder().user(userRepository.findUserByName(userDTO.getName()).get()).word(wordRepository.findWordByNameAndDescription(wordDTO.getName(),wordDTO.getDescription()).get()).attempts(wordDTO.getName().length()).points(0).user_word(userWord).build();
        gameRepository.save(game);
        return GameDTO.from(game);
    }

    public GameDTO failGuess(Integer gameId){
        Game game = gameRepository.findById(gameId).get();
        game.setAttempts(game.getAttempts()-1);
        String[] letters = game.getAlphabet().split("");
        game.setAlphabet(Arrays.stream(letters).collect(Collectors.joining()));
        gameRepository.save(game);

        return GameDTO.from(game);
    }

    public GameDTO successGuess(String letter, Integer gameId){
        Game game = gameRepository.findById(gameId).get();
        String[] letters = game.getAlphabet().split("");
        String[] word = game.getWord().getName().split("");
        String[] userWord =  game.getUser_word().split("");
//        -Remove successfull guessed letters-
        for (int i=0; i<letters.length; i++) {
        if(letters[i].equals(letter)) {
            letters[i] = "";
        }}
        for (int i=0; i<word.length; i++){
        if(word[i].toUpperCase().equals(letter)) {
            userWord[i] = letter;
            game.setAttempts(game.getAttempts()-1);
            game.setPoints(game.getPoints()+1);
        }}
        game.setAlphabet(Arrays.stream(letters).collect(Collectors.joining()));
        game.setUser_word(Arrays.stream(userWord).collect(Collectors.joining()));


        gameRepository.save(game);

        return GameDTO.from(game);
    }

}
