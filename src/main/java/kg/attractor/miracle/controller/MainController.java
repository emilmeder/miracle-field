package kg.attractor.miracle.controller;

import kg.attractor.miracle.dto.*;
import kg.attractor.miracle.service.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;

@Controller
@AllArgsConstructor
public class MainController {
    private UserService userService;
    private WordService wordService;
    private GameService gameService;

//    Главная страница после авторизации
    @GetMapping("/")
    public String indexPage(Model model, Principal principal) {
        UserDTO userDTO = userService.findUserByName(principal.getName());
//        var WordModel = model.addAttribute("allWords", wordService.findAllWords());
        if (userDTO.getRole().equals("ROLE_ADMIN")){
            model.addAttribute("admin", true);
        }
        else {
            model.addAttribute("user", true);
        }
        return "index";
    }

//    Страницы профиля, всех пользователей, слов, и статистики
    @GetMapping("/profile")
    public String userProfile(Model model, Principal principal){
        UserDTO userDTO = userService.findUserByName(principal.getName());
        model.addAttribute("userDTO", userDTO);
        return "profile";
    }

    @GetMapping("/allUsers")
    public String allUsersPage(Model model){
        model.addAttribute("allUsers", userService.findAllUsers());
        return "allUsers";
    }

    @GetMapping("/allWords")
    public String allWordsPage(Model model){
        model.addAttribute("allWords", wordService.findAllWords());
        return "allWords";
    }

    @GetMapping("/statistic")
    public String allGamesPage(Model model){
        model.addAttribute("allGames", gameService.findAllGames());
        return "statistic";
    }
//    Добавление пользователей и новых слов
    @PostMapping("/addUser")
    public String addUser(UserDTO userDTO) {
        userService.addUser(userDTO);
        return "redirect:/";
    }

    @PostMapping("/addWord")
    public String addWord(WordDTO wordDTO) {
        wordService.addWord(wordDTO);
        return "redirect:/";
    }

//    Страница самой игры
    @GetMapping("/game")
    public String game(Model model, Principal principal) {
        UserDTO userDTO = userService.findUserByName(principal.getName());
        WordDTO wordDTO = wordService.randomWord();
        GameDTO gameDTO = gameService.saveGame(userDTO,wordDTO);
        model.addAttribute("user",userDTO);
        model.addAttribute("game",gameDTO);
        model.addAttribute("word_letters",gameDTO.getUser_word().split(""));
        model.addAttribute("alphabet",gameDTO.getAlphabet().split(""));
        return "gamePage";
    }

    @GetMapping("/gaming")
    public String gaming(){
        return "gamePage";
    }

//    Обработка угадывания букв пользователем
    @PostMapping("/play")
    public String play(@RequestParam("letter") String letter, @RequestParam("game_id") Integer game_id, RedirectAttributes attributes) throws RuntimeException {
        GameDTO gameDTO = gameService.findGameById(game_id);
        if (gameDTO.getWordDTO().getName().toUpperCase().indexOf(letter)>=0){
            GameDTO gameDTO2 = gameService.successGuess(letter, game_id);
            if(gameDTO2.getWordDTO().getName().toUpperCase().equals(gameDTO2.getUser_word())){
                attributes.addFlashAttribute("points",gameDTO2.getPoints());
                return "redirect:/success";
            }
            attributes.addFlashAttribute("game",gameDTO2);
            attributes.addFlashAttribute("alphabet",gameDTO2.getAlphabet().split(""));
            attributes.addFlashAttribute("word_letters",gameDTO2.getUser_word().split(""));
            return "redirect:/gaming";
        }
        else {
            if (gameDTO.getAttempts()>0){
                GameDTO gameDTO1 = gameService.failGuess(game_id);
                attributes.addFlashAttribute("game",gameDTO1);
                attributes.addFlashAttribute("alphabet",gameDTO1.getAlphabet().split(""));
                attributes.addFlashAttribute("word_letters",gameDTO1.getUser_word().split(""));
                return "redirect:/gaming";
            }
            else {
                attributes.addFlashAttribute("points",gameDTO.getPoints());
                return "redirect:/fail";
            }
        }
    }

//    Страницы выигрыша и проигрыша
    @GetMapping("/success")
    public String success(){

        return "success";
    }

    @GetMapping("/fail")
    public String fail(){

        return "fail";
    }


}
