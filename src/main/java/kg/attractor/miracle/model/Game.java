package kg.attractor.miracle.model;
import javax.persistence.*;
import lombok.*;
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Table(name = "games")
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private Integer attempts;

    @Column
    private Integer points;

    @Column
    @Builder.Default
    private String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    @Column(length = 128)
    private String user_word;

    @ManyToOne
    @JoinColumn(name = "word_id")
    private Word word;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

}
