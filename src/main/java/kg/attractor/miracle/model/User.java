package kg.attractor.miracle.model;
import lombok.*;
import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 128)
    private String name;

    @Column(length = 128)
    private String password;

    @Column
    @Builder.Default
    private Boolean enabled = true;

    @Column(length = 128)
    @Builder.Default
    private String role = "ROLE_USER";

}
