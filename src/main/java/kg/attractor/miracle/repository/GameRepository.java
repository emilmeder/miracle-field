package kg.attractor.miracle.repository;
import java.util.List;
import kg.attractor.miracle.model.*;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.criteria.CriteriaBuilder;


public interface GameRepository extends JpaRepository<Game, Integer> {
    List<Game> findGamesByUserId(Integer id);

    List<Game> findGamesByWordId(Integer id);

    List<Game> findGameByUserId(Integer id);

}
