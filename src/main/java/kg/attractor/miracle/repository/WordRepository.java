package kg.attractor.miracle.repository;
import kg.attractor.miracle.model.Word;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface WordRepository extends JpaRepository<Word, Integer> {
    Optional<Word> findByName(String name);

    Optional<Word> findByDescription(String description);

    Optional<Word> findWordByNameAndDescription(String name, String description);
}
