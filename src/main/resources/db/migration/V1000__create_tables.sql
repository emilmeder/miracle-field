CREATE TABLE users (
    id SERIAL PRIMARY KEY,

    name VARCHAR(128) NOT NULL,

    password VARCHAR(128) NOT NULL,

    enabled boolean NOT NULL DEFAULT TRUE,

    role VARCHAR(128) NOT NULL DEFAULT 'ROLE_USER'
);

CREATE TABLE words (
    id SERIAL PRIMARY KEY,

    name VARCHAR(128) NOT NULL,

    description VARCHAR(128) NOT NULL
);

CREATE TABLE games (
    id SERIAL PRIMARY KEY,

    attempts INT NOT NULL,

    points INT NOT NULL,

    alphabet VARCHAR(128) NOT NULL,

    user_word VARCHAR(128) NOT NULL,

    word_id INT NOT NULL,

    user_id INT NOT NULL,

    FOREIGN KEY (user_id) REFERENCES users (id),

    FOREIGN KEY (word_id) REFERENCES words (id)
);

-- INSERT INTO users value ('pew', '123')