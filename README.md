Игра "Поле чудес"

Технологии: Sring Boot, PostgreSQL, java 13, IntelliJ IDEA, Apache Freemarker, Apache Maven

Инструкция:

Открыть проект в IntelliJ IDEA
* Создать схему(базу) в PostgreSQL с названием miracle(или настроить название базы в application.properties)
* в application.properties ввести:
* spring.datasource.url - путь к базе данных
* spring.datasource.username - имя пользователя в базе данных
* spring.datasource.password - пароль
* запустить проект по http://localhost:8080/
* Тестовые данные созданы в PreloadDatabase:

Админ:

* имя: admin
* пароль: 123


Пользователей можно посмотреть по кнопке на главной странице

Пароль всех тестовых пользователей: 123